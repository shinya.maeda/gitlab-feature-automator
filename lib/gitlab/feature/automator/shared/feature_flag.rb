module Gitlab
  module Feature
    module Automator
      module Shared
        class FeatureFlag
          attr_reader :definition

          AGED_MILESTONE_THRETHOLD = '0.1'

          def initialize(definition)
            @definition = definition
          end

          def cleanable?
          end

          def rollout_issue_iid
            
          end

          def introduced_by_iid
            
          end

          def name
            definition['name']
          end

          def overridden?
            definition['overridden']
          end

          def aged?
            Gitlab.current_milestone > (milestone + AGED_MILESTONE_THRETHOLD)
          end

          private

          def milestone
            definition['milestone']
          end
        end
      end
    end
  end
end
