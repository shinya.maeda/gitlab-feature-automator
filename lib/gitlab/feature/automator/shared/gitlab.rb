module Gitlab
  module Feature
    module Automator
      module Shared
        class Gitlab
          ENVIRONMENTS = {
            production: 'https://gitlab.com',
            staging: 'https://staging.gitlab.com',
            dev: 'https://dev.gitlab.org'
          }.freeze

          class << self
            def current_milestone
              return @current_milestone if defined?(@current_milestone)

              # TODO: Read blob at VERSION on master HEAD gitlab canonical repo

              @current_milestone = 
            end
          end
        end
      end
    end
  end
end
