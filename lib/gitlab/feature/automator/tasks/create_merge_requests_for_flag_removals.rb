require 'httparty'
require 'pry'
require 'feature_flag'

module Gitlab
  module Feature
    module Automator
      module Tasks
        class CreateMergeRequestsForFlagRemovals < Base
          DEFAULT_TARGET_BRANCH = 'master'

          def execute
            cleanable_flags.select(&:aged?).each do |flag|
              callout_in_rollout_issue(flag,
                "Creating an merge request for flag #{flag.name} removal ...")

              create_feature_branch(flag)
              create_commit_to_remove_flag_yaml(flag)
              merge_request = create_merge_request(flag, source_branch)

              callout_in_rollout_issue(flag,
                "Created an merge request for flag removal. " \
                "Assigned to #{creator_of_feature_flag(flag).username}. " \
                "Merge Request URL: #{merge_request.web_url}")
            end
          end

          private

          def create_feature_branch(flag)
            response = HTTParty.post(
              "#{api_v4_gitlab_path}/repository/branches",
              body: {
                branch: branch_name(flag),
                ref: DEFAULT_TARGET_BRANCH
              },
              headers: api_headers)
          end

          def create_commit_to_remove_flag_yaml(flag)
            response = HTTParty.post(
              "#{api_v4_gitlab_path}/repository/commits",
              body: {
                branch: branch_name(flag),
                commit_message: removal_title(flag),
                actions: [
                  {
                    action: :delete,
                    file_path: flag.path
                  }
                ]
              },
              headers: api_headers)
          end

          def create_merge_request(flag)
            response = HTTParty.post(
              "#{api_v4_gitlab_path}/merge_requests",
              body: {
                target_branch: DEFAULT_TARGET_BRANCH,
                source_branch: branch_name(flag),
                title: removal_title(flag),
                labels: ['feature flag', 'automations'],
                assignee_ids: [creator_of_feature_flag(flag).id]
              },
              headers: api_headers)
          end

          def branch_name(flag)
            "gitlab-featureflag-automator/removal/#{flag.name}"
          end

          def removal_title(flag)
            "Remove a feature flag #{flag.name}"
          end
        end
      end
    end
  end
end
