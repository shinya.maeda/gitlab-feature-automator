require 'base'

module Gitlab
  module Feature
    module Automator
      class CloseRolloutIssues < Base
        def execute
          cleanable_flags.each do |flag|
            callout_in_rollout_issue(flag, "Closing the rollout issue for #{flag.name} ...")
            close_rollout_issue(flag)
          end
        end

        private

        def close_rollout_issue(flag)
          response = HTTParty.put(
            "#{api_v4_gitlab_path}/issues/#{flag.rollout_issue_iid}",
            body: { state_event: :close },
            headers: api_headers)
        end
      end
    end
  end
end
