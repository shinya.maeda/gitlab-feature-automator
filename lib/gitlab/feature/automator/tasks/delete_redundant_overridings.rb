require 'base'

module Gitlab
  module Feature
    module Automator
      module Tasks
        class DeleteRedundantOverridings < Base
          def execute
            cleanable_flags.select(&:overridden?).each do |flag|
              callout_in_rollout_issue(flag, "Deleting redundant overridings for the flag #{flag.name} ...")
              delete_overriding(flag)
            end
          end

          private

          def delete_overriding(flag)
            response = HTTParty.delete(
              "#{api_v4_feature_flag_path}/#{flag.name}",
              headers: api_headers)
          end
        end
      end
    end
  end
end
