require 'gitlab/featureflag/automator/shared/feature_flag'
require 'gitlab/featureflag/automator/shared/gitlab'
require 'httparty'

module Gitlab
  module Feature
    module Automator
      module Tasks
        class Base
          attr_reader :params

          def initialize(params)
            @params = params
          end

          def execute
            raise NotImplementedError
          end

          def cleanable_flags
            return @cleanable_flags if defined?(@cleanable_flags)
        
            response = HTTParty.get(
              "#{api_v4_path}/features/definitions?auto_clean_up=true",
              headers: api_headers)
        
            @cleanable_flags = response.body.map { |definition| FeatureFlag.new(definition) }
          end

          def callout_in_rollout_issue(flag, message)
            response = HTTParty.post(
              "#{api_v4_path}/projects/#{Config.gitlab_canonical_project_path}/issues/#{flag.rollout_issue_iid}/notes",
              body: message,
              headers: api_headers)
          end

          def creator_of_feature_flag(flag)
            return @creator_of_feature_flag[flag.name] if defined?(@creator_of_feature_flag) && @creator_of_feature_flag[flag.name]

            @creator_of_feature_flag ||= {}

            response = HTTParty.post(
              "#{api_v4_gitlab_path}/merge_requests/#{flag.introduced_by_iid}",
              body: message,
              headers: api_headers)

            @creator_of_feature_flag[flag.name] = response.author
          end

          def api_v4_path
            "#{Config.gitlab_url}/api/v4"
          end

          def api_v4_gitlab_path
            "#{api_v4_path}/projects/#{Config.gitlab_canonical_project_path}"
          end

          def api_v4_feature_flag_path
            "#{api_v4_path}/features"
          end

          def api_headers
            {
              'Private-token' => Config.private_token
            }
          end
        end
      end
    end
  end
end
