module Gitlab
  module Feature
    module Automator
      module Config
        OPTIONS_KEYS = %i[
          gitlab_url
          private_token
          gitlab_canonical_project_path
        ].freeze

        attr_accessor(*OPTIONS_KEYS)

        def self.extended(base)
          base.reset
        end

        def configure
          yield self
        end

        def options
          OPTIONS_KEYS.inject({}) do |option, key|
            option.merge!(key => send(key))
          end
        end

        def reset
          self.gitlab_url              = ENV['GITLAB_URL']
          self.gitlab_private_token    = ENV['GITLAB_PRIVATE_TOKEN']
          self.canonical_project_path  = ENV['GITLAB_CANONICAL_PROJECT_PATH']
        end
      end
    end
  end
end
