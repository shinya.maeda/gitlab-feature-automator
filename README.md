# Gitlab::Feature::Automator (WIP)

This project provides CLI/gem for automating [Feature flags in development of GitLab](https://docs.gitlab.com/ee/development/feature_flags/).

## Tasks

#### Auto Clean Up

These tasks are activated for the feature flags that [defines](https://docs.gitlab.com/ee/development/feature_flags/development.html#feature-flag-definition-and-validation)  `auto_clean_up: true`.

##### Delete Redundant Overridings

##### Create Merge Requests For Flag Removals

##### Close Feature Flag Rollout Issues

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-feature-automator'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install gitlab-feature-automator

## Configuration

```
Gitlab::Featureflag::Automator::Config do |config|
  config.gitlab_canonical_project_url = ''
  config.gitlab_canonical_project_api_token = ''
  config.gitlab_production_url = ''
  config.gitlab_production_api_token = ''
  config.gitlab_staging_url = ''
  config.gitlab_staging_api_token = ''
  config.gitlab_dev_url = ''
  config.gitlab_dev_api_token = ''
end
```

## Usage

### CLI

```
bin/automator --help
```

```
bin/automator exec --tasks=close_rollout_issues
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/gitlab-feature-automator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/gitlab-feature-automator/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Gitlab::Feature::Automator project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/gitlab-feature-automator/blob/master/CODE_OF_CONDUCT.md).
